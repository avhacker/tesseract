#ifndef   LIST_HELPERS_H
#define   LIST_HELPERS_H

namespace helpers
{
  template <typename List, typename OP>
  void Traverse(List &list, OP &op)
  {
    typename List::iterator it(&list);
    for (it.mark_cycle_pt(); !it.cycled_list(); it.forward())
    {
      op(it);
    }
  }

  template <typename List, typename OP>
  void Traverse(List &list, const OP &op)
  {
    typename List::iterator it(&list);
    for (it.mark_cycle_pt(); !it.cycled_list(); it.forward())
    {
      op(it);
    }
  }

  template <typename List, typename OP>
  typename List::iterator Find(List &list, OP &op)
  {
    typedef typename List::iterator iterator_t;
    iterator_t it(&list);
    for (it.mark_cycle_pt(); !it.cycled_list(); it.forward())
    {
      if ( op(it) )
        return it;
    }
    return iterator_t();
  }

  template <typename List, typename OP>
  typename List::iterator Find(List &list, const OP &op)
  {
    typedef typename List::iterator iterator_t;
    iterator_t it(&list);
    for (it.mark_cycle_pt(); !it.cycled_list(); it.forward())
    {
      if ( op(it) )
        return it;
    }
    return iterator_t();
  }

  template <typename List>
  unsigned CountList(List &list)
  {
    struct Counter{
      Counter() : count_(0){}
      void operator()(typename List::iterator) {++ count_;}
      unsigned Count() const {return count_;}
    private:
      unsigned count_;
    };

    Counter counter;
    Traverse(list, counter);
    return counter.Count();
  }
}

#endif
