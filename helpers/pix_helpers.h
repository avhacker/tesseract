#ifndef   PIX_HELPERS_H
#define   PIX_HELPERS_H

#include "pix.h"
#include "leptprotos.h"

namespace helpers
{
  inline PIX* CreateSubImage(PIX* src, BOX* box)
  {
    BOXA *boxa = boxaCreate(1);
    boxa->box[0] = box;
    boxChangeRefcount(box, 1);
    boxa->n = 1;
    PIXA *pixa = pixaCreateFromBoxa(src, boxa, NULL);
    PIX* subpix = pixa->pix[0];
    pixChangeRefcount(subpix, 1);
    pixaDestroy(&pixa);
    boxaDestroy(&boxa);
    return subpix;
  }

  inline PIX* CreateSubImage(PIX* src, int x, int y, int width, int height)
  {
    BOX* box = boxCreate(x, y, width, height);
    PIX* subpix = CreateSubImage(src, box);
    boxDestroy(&box);
    return subpix;
  }

  inline PIX* CreateSubImage(PIX* src, const TBOX &tbox)
  {
    return CreateSubImage(src, tbox.left(), src->h - tbox.top(), tbox.width(), tbox.height() );
  }

}

#endif
