#ifndef   STRUCTURE_HELPERS_H
#define   STRUCTURE_HELPERS_H

#include "pix.h"
#include "leptprotos.h"
#include "rect.h"
#include "elst.h"
#include "elst2.h"
#include "blobbox.h"
#include "pix_helpers.h"
#include "list_helpers.h"

#include <string>
#include <sstream>
#include <iomanip>

ELISTIZE (BLOBNBOX);
ELIST2IZE(WERD);

namespace helpers
{

  inline void WriteTiff(const std::string& file_name, PIX* pix)
  {
    pixWriteTiff(file_name.c_str(), pix, IFF_TIFF, "w");
  }

  struct IteratorTracer{
    IteratorTracer(const std::string& prefix) : index_(0), prefix_(prefix){}
    std::string GetStr() const{
      ++index_;
      std::ostringstream oss;
      oss << prefix_ << std::setw(4) << std::setfill('0') << index_;
      return oss.str();
    }

  private:
    mutable unsigned index_;
    std::string prefix_;
  };

  struct IteratorTracerWithPix : public IteratorTracer{
    IteratorTracerWithPix(const std::string& prefix, PIX* pix) : IteratorTracer(prefix){
      pix_ = pixClone(pix);
      pixChangeRefcount(pix_, 1);
    }
    ~IteratorTracerWithPix(){
      pixDestroy(&pix_);
    }
    PIX* GetPix() const{ return pix_; }
  private:
    PIX *pix_;
  };

  struct BLOBNBOX_Dump : public IteratorTracerWithPix{
    BLOBNBOX_Dump(const std::string& prefix, PIX* pix) : IteratorTracerWithPix(prefix + "blob", pix) {}
    void operator()(BLOBNBOX_IT it) const
    {
      const TBOX &tbox = it.data()->bounding_box();
      PIX* pix = helpers::CreateSubImage(GetPix(), tbox);
      WriteTiff((GetStr() + ".tif").c_str(), pix);
      pixDestroy(&pix);
    }
  };

  struct TO_BLOCK_Dump : public IteratorTracerWithPix{
    TO_BLOCK_Dump(const std::string& prefix, PIX* pix) : IteratorTracerWithPix(prefix + "to_block", pix) {}
    void operator()(TO_BLOCK_IT it) const
    {
      helpers::Traverse(it.data()->blobs, BLOBNBOX_Dump(GetStr(), GetPix()));
    }
  };

  inline void MergeRect(TBOX& to, const TBOX& from)
  {
    to.set_left(from.left() < to.left() ? from.left() : to.left());
    to.set_right(from.right() > to.right() ? from.right() : to.right());
    to.set_bottom(from.bottom() < to.bottom() ? from.bottom() : to.bottom());
    to.set_top(from.top() > to.top() ? from.top() : to.top());
  }

  struct Outline_Rect_Builder{
    Outline_Rect_Builder() : init_(false) {}
    void operator()(C_OUTLINE_IT it){
      C_OUTLINE *outline = it.data();
      const TBOX &tbox = outline->bounding_box();
      if ( !init_ ){
        init_ = true;
        tbox_ = tbox;
      }
      else
      {
        MergeRect(tbox_, tbox);
      }
    }
    const TBOX& GetBox() const { return tbox_; }
  private:
    bool init_;
    TBOX tbox_;
  };

  inline TBOX GetRect(C_BLOB* blob)
  {
    Outline_Rect_Builder rect_builder;
    helpers::Traverse(*blob->out_list(), rect_builder);
    const TBOX &box = rect_builder.GetBox();
    return box;
  }

  struct C_BLOB_Dump : public IteratorTracerWithPix{
    C_BLOB_Dump(const std::string& prefix, PIX* pix) : IteratorTracerWithPix(prefix + "C_BLOB", pix) {}
    void operator()(C_BLOB_IT it) const
    {
      C_BLOB *blob = it.data();
      C_OUTLINE_IT out_line_it(blob->out_list());
      C_OUTLINE *out_line = out_line_it.data();
      const TBOX &tbox = out_line->bounding_box();
      PIX* pix = helpers::CreateSubImage(GetPix(), tbox);
      std::ostringstream oss;
      oss << GetStr() << "-" << tbox.left() << ", " << tbox.top() << ", " << tbox.width() << ", " << tbox.height() << ".tif";
      WriteTiff(oss.str().c_str(), pix);
      pixDestroy(&pix);
    }
  };

  struct WERD_Rect_Builder{
    WERD_Rect_Builder() : init_(false) {}
    void operator()(C_BLOB_IT it){
      TBOX tbox = GetRect(it.data());
      if ( !init_ ){
        init_ = true;
        tbox_ = tbox;
      }
      else
      {
        MergeRect(tbox_, tbox);
      }
    }
    const TBOX& GetBox() const { return tbox_; }
  private:
    bool init_;
    TBOX tbox_;
  };

  struct WERD_Dump : public IteratorTracerWithPix{
    WERD_Dump(const std::string& prefix, PIX* pix) : IteratorTracerWithPix(prefix + "WERD", pix) {}
    void operator()(WERD_IT it) const
    {
      helpers::Traverse(*(it.data()->cblob_list()), C_BLOB_Dump(GetStr(), GetPix()));
    }
  };

  inline TBOX GetRect(WERD* word)
  {
    WERD_Rect_Builder rect_builder;
    C_BLOB_LIST *plist = word->cblob_list();
    helpers::Traverse(*plist, rect_builder);
    const TBOX &box = rect_builder.GetBox();
    return box;
  }

  struct WERD_Self_Dump : public IteratorTracerWithPix{
    WERD_Self_Dump(const std::string& prefix, PIX* pix) : IteratorTracerWithPix(prefix + "WERD", pix) {}
    void operator()(WERD_IT it) const
    {
      TBOX box = GetRect(it.data());
      if ( !box.null_box() )
      {
        PIX* pix = helpers::CreateSubImage(GetPix(), box);
        WriteTiff(GetStr() + ".tif", pix);
        pixDestroy(&pix);
      }
    }
  };

  struct ROW_Dump : public IteratorTracerWithPix{
    ROW_Dump(const std::string& prefix, PIX* pix) : IteratorTracerWithPix(prefix + "row", pix) {}
    void operator()(ROW_IT it) const
    {
      helpers::Traverse(*(it.data()->word_list()), WERD_Self_Dump(GetStr(), GetPix()));
    }
  };

  struct BLOCK_Dump : public IteratorTracerWithPix{
    BLOCK_Dump(const std::string& prefix, PIX* pix) : IteratorTracerWithPix(prefix + "block", pix) {}
    void operator()(BLOCK_IT it) const
    {
      helpers::Traverse(*(it.data()->row_list()), ROW_Dump(GetStr(), GetPix()));
    }
  };

  struct WerdContainsCoord{
    WerdContainsCoord(const ICOORD &coord) : coord_(coord){}
    bool operator()(WERD_IT it) const{
      TBOX tbox = GetRect(it.data());
      if ( tbox.left() <= coord_.x() &&
           tbox.right() >= coord_.x() &&
           tbox.bottom() <= coord_.y() &&
           tbox.top() >= coord_.y())
        return true;
      return false;
    }
    bool operator()(WERD* word) const{
      TBOX tbox = GetRect(word);
      if ( tbox.left() <= coord_.x() &&
        tbox.right() >= coord_.x() &&
        tbox.bottom() <= coord_.y() &&
        tbox.top() >= coord_.y())
        return true;
      return false;
    }
  private:
    ICOORD coord_;
  };

  inline WERD_IT FindWerd(WERD_LIST &words, const ICOORD &coord)
  {
    return Find(words, WerdContainsCoord(coord));
  }

  inline WERD_IT FindWerd(ROW_LIST &rows, const ICOORD &coord)
  {
    ROW_IT it(&rows);
    WerdContainsCoord finder(coord);
    for (it.mark_cycle_pt(); !it.cycled_list(); it.forward())
    {
      WERD_IT word_it = Find(*(it.data()->word_list()), finder);
      if ( word_it.data() != NULL )
        return word_it;
    }
    return WERD_IT();
  }

}

#endif
