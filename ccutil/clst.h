/**********************************************************************
 * File:        clst.h  (Formerly clist.h)
 * Description: CONS cell list module include file.
 * Author:      Phil Cheatle
 * Created:     Mon Jan 28 08:33:13 GMT 1991
 *
 * (C) Copyright 1991, Hewlett-Packard Ltd.
 ** Licensed under the Apache License, Version 2.0 (the "License");
 ** you may not use this file except in compliance with the License.
 ** You may obtain a copy of the License at
 ** http://www.apache.org/licenses/LICENSE-2.0
 ** Unless required by applicable law or agreed to in writing, software
 ** distributed under the License is distributed on an "AS IS" BASIS,
 ** WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 ** See the License for the specific language governing permissions and
 ** limitations under the License.
 *
 **********************************************************************/

#ifndef CLST_H
#define CLST_H

#include <stdio.h>
#include "host.h"
#include "serialis.h"
#include "lsterr.h"

template <typename T>
class CLIST_ITERATOR;

/**********************************************************************
 *							CLASS - CLIST_LINK
 *
 *							Generic link class for singly linked CONS cell lists
 *
 *  Note:  No destructor - elements are assumed to be destroyed EITHER after
 *  they have been extracted from a list OR by the CLIST destructor which
 *  walks the list.
 **********************************************************************/

template <typename T>
class DLLSYM CLIST_LINK
{
  typedef CLIST_LINK<T> CLIST_LINKype_;
  template<typename U>
  friend class CLIST_ITERATOR;

  template<typename U>
  friend class CLIST;

  CLIST_LINKype_ *next;
  T *data;

  public:
	  CLIST_LINK() : next(NULL), data(NULL) {  //constructor
    }

    CLIST_LINK(                       //copy constructor
               const CLIST_LINKype_ &) : next(NULL), data(NULL) {  //dont copy link
    }

    CLIST_LINK& operator= (             //dont copy links
    const CLIST_LINKype_ &) {
		next = NULL;
		data = NULL;
    }
};

/**********************************************************************
 * CLASS - CLIST
 *
 * Generic list class for singly linked CONS cell lists
 **********************************************************************/

template <typename T>
class DLLSYM CLIST
{
  typedef CLIST<T> CLISType_;
  template <typename U>
  friend class CLIST_ITERATOR;

  CLIST_LINK<T> *last;              //End of list
  //(Points to head)
  CLIST_LINK<T> *First() {  // return first
    return last != NULL ? last->next : NULL;
  }

  CLIST(const CLISType_&);  // prohibited
  CLIST& operator=(const CLISType_&); //prohibited

  public:
    typedef CLIST_ITERATOR<T> iterator;

    CLIST() :last(NULL){  //constructor
    }

    ~CLIST () {                  //destructor
      shallow_clear();
    }

    void deep_clear (){   //destroy all links
      CLIST_LINK<T> *ptr;
      CLIST_LINK<T> *next;

#ifndef NDEBUG
      if (!this)
        NULL_OBJECT.error ("CLIST::deep_clear", ABORT, NULL);
#endif

      if (!empty ()) {
        ptr = last->next;            //set to first
        last->next = NULL;           //break circle
        last = NULL;                 //set list empty
        while (ptr) {
          next = ptr->next;
          delete ptr->data;
          delete(ptr);
          ptr = next;
        }
      }

    }

    void shallow_clear();  //clear list but dont
    //delete data elements

    bool empty() const {  //is list empty?
      return !last;
    }

    bool singleton() const {
      return last != NULL ? (last == last->next) : false;
    }

    void shallow_copy(                      //dangerous!!
                      CLISType_ *from_list) {  //beware destructors!!
      last = from_list->last;
    }

    void deep_copy (const CLISType_ * list){  //list being copied
      CLIST_ITERATOR<T> from_it ((CLIST *) list);
      CLIST_ITERATOR<T> to_it(this);

#ifndef NDEBUG
      if (!this)
        NULL_OBJECT.error ("CLIST::deep_copy", ABORT, NULL);
      if (!list)
        BAD_PARAMETER.error ("CLIST::deep_copy", ABORT,
        "source list is NULL");
#endif

      for (from_it.mark_cycle_pt (); !from_it.cycled_list (); from_it.forward ())
      {
        T* new_element = new T(*from_it.data());
        to_it.add_after_then_move(new_element);
      }
    }

    void assign_to_sublist(                           //to this list
                           CLIST_ITERATOR<T> *start_it,  //from list start
                           CLIST_ITERATOR<T> *end_it);   //from list end

    inT32 length() const;  //# elements in list

    void sort (                  //sort elements
      int comparator (           //comparison routine
      const void *, const void *));

    // Assuming list has been sorted already, insert new_data to
    // keep the list sorted according to the same comparison function.
    // Comparision function is the same as used by sort, i.e. uses double
    // indirection. Time is O(1) to add to beginning or end.
    // Time is linear to add pre-sorted items to an empty list.
    // If unique, then don't add duplicate entries.
    // Returns true if the element was added to the list.
    bool add_sorted(int comparator(const void*, const void*),
                    bool unique, T* new_data);

    // Assuming that the minuend and subtrahend are already sorted with
    // the same comparison function, shallow clears this and then copies
    // the set difference minuend - subtrahend to this, being the elements
    // of minuend that do not compare equal to anything in subtrahend.
    // If unique is true, any duplicates in minuend are also eliminated.
    void set_subtract(int comparator(const void*, const void*), bool unique,
                      CLISType_* minuend, CLISType_* subtrahend);

};

/***********************************************************************
 *							CLASS - CLIST_ITERATOR
 *
 *							Generic iterator class for singly linked lists with embedded links
 **********************************************************************/

 template <typename T>
class DLLSYM CLIST_ITERATOR
{
  typedef CLIST_ITERATOR<T> CLIST_IT_Type_;
  template <typename U>
  friend void CLIST<U>::assign_to_sublist(CLIST_ITERATOR<U> *, CLIST_ITERATOR<U> *);

  CLIST<T> *list;                   //List being iterated
  CLIST_LINK<T> *prev;              //prev element
  CLIST_LINK<T> *current;           //current element
  CLIST_LINK<T> *next;              //next element
  BOOL8 ex_current_was_last;     //current extracted
  //was end of list
  BOOL8 ex_current_was_cycle_pt; //current extracted
  //was cycle point
  CLIST_LINK<T> *cycle_pt;          //point we are cycling
  //the list to.
  BOOL8 started_cycling;         //Have we moved off
  //the start?

  CLIST_LINK<T> *extract_sublist(                            //from this current...
                              CLIST_IT_Type_ *other_it);  //to other current

  public:
      CLIST_ITERATOR() :list(NULL){  //constructor
    }                            //unassigned list

    CLIST_ITERATOR(  //constructor
                   CLIST<T> *list_to_iterate);

    void set_to_list(  //change list
                     CLIST<T> *list_to_iterate);

    void add_after_then_move(                  //add after current &
                             T *new_data);  //move to new

    void add_after_stay_put(                  //add after current &
                            T *new_data);  //stay at current

    void add_before_then_move(                  //add before current &
                              T *new_data);  //move to new

    void add_before_stay_put(                  //add before current &
                             T *new_data);  //stay at current

    void add_list_after(                      //add a list &
                        CLIST<T> *list_to_add);  //stay at current

    void add_list_before(                      //add a list &
                         CLIST<T> *list_to_add);  //move to it 1st item

    T *data() {  //get current data
    #ifndef NDEBUG
      if (!list)
        NO_LIST.error ("CLIST_ITERATOR::data", ABORT, NULL);
      if (!current)
        NULL_DATA.error ("CLIST_ITERATOR::data", ABORT, NULL);
    #endif
      return current->data;
    }

    T *data_relative(               //get data + or - ...
                        inT8 offset);  //offset from current

    T *forward();  //move to next element

    T *extract();  //remove from list

    T *move_to_first();  //go to start of list

    T *move_to_last();  //go to end of list

    void mark_cycle_pt();  //remember current

    BOOL8 empty() {  //is list empty?
    #ifndef NDEBUG
      if (!list)
        NO_LIST.error ("CLIST_ITERATOR::empty", ABORT, NULL);
    #endif
      return list->empty ();
    }

    BOOL8 current_extracted() {  //current extracted?
      return !current;
    }

    BOOL8 at_first();  //Current is first?

    BOOL8 at_last();  //Current is last?

    BOOL8 cycled_list();  //Completed a cycle?

    void add_to_end(                  //add at end &
                    T *new_data);  //dont move

    void exchange(                            //positions of 2 links
                  CLIST_IT_Type_ *other_it);  //other iterator

    inT32 length();  //# elements in list

    void sort (                  //sort elements
      int comparator (           //comparison routine
      const void *, const void *));

};

/***********************************************************************
 *  MEMBER FUNCTIONS OF CLASS: CLIST<T>
 *  ================================
 **********************************************************************/

 /***********************************************************************
 *							CLIST<T>::shallow_clear
 *
 *  Used by the destructor and the "shallow_clear" member function of derived
 *  list classes to destroy the list.
 *  The data elements are NOT destroyed.
 *
 **********************************************************************/

template <typename T>
void CLIST<T>::shallow_clear() {  //destroy all links
  CLIST_LINK<T> *ptr;
  CLIST_LINK<T> *next;

  #ifndef NDEBUG
  if (!this)
    NULL_OBJECT.error ("CLIST<T>::shallow_clear", ABORT, NULL);
  #endif

  if (!empty ()) {
    ptr = last->next;            //set to first
    last->next = NULL;           //break circle
    last = NULL;                 //set list empty
    while (ptr) {
      next = ptr->next;
      delete(ptr);
      ptr = next;
    }
  }
}


/***********************************************************************
 *							CLIST<T>::assign_to_sublist
 *
 *  The list is set to a sublist of another list.  "This" list must be empty
 *  before this function is invoked.  The two iterators passed must refer to
 *  the same list, different from "this" one.  The sublist removed is the
 *  inclusive list from start_it's current position to end_it's current
 *  position.  If this range passes over the end of the source list then the
 *  source list has its end set to the previous element of start_it.  The
 *  extracted sublist is unaffected by the end point of the source list, its
 *  end point is always the end_it position.
 **********************************************************************/

template <typename T>
void CLIST<T>::assign_to_sublist(                           //to this list
                              CLIST_ITERATOR<T> *start_it,  //from list start
                              CLIST_ITERATOR<T> *end_it) {  //from list end
  const ERRCODE LIST_NOT_EMPTY =
    "Destination list must be empty before extracting a sublist";

  #ifndef NDEBUG
  if (!this)
    NULL_OBJECT.error ("CLIST<T>::assign_to_sublist", ABORT, NULL);
  #endif

  if (!empty ())
    LIST_NOT_EMPTY.error ("CLIST<T>.assign_to_sublist", ABORT, NULL);

  last = start_it->extract_sublist (end_it);
}


/***********************************************************************
 *							CLIST<T>::length
 *
 *  Return count of elements on list
 **********************************************************************/

template <typename T>
inT32 CLIST<T>::length() const {  //count elements
  CLIST_ITERATOR<T> it(const_cast<CLISType_*>(this));
  inT32 count = 0;

  #ifndef NDEBUG
  if (!this)
    NULL_OBJECT.error ("CLIST<T>::length", ABORT, NULL);
  #endif

  for (it.mark_cycle_pt(); !it.cycled_list(); it.forward())
    count++;
  return count;
}


/***********************************************************************
 *							CLIST<T>::sort
 *
 *  Sort elements on list
 **********************************************************************/

template <typename T>
void
CLIST<T>::sort (                    //sort elements
int comparator (                 //comparison routine
const void *, const void *)) {
  CLIST_ITERATOR<T> it(this);
  inT32 count;
  void **base;                   //ptr array to sort
  void **current;
  inT32 i;

  #ifndef NDEBUG
  if (!this)
    NULL_OBJECT.error ("CLIST<T>::sort", ABORT, NULL);
  #endif

  /* Allocate an array of pointers, one per list element */
  count = length ();
  base = (void **) malloc (count * sizeof (void *));

  /* Extract all elements, putting the pointers in the array */
  current = base;
  for (it.mark_cycle_pt (); !it.cycled_list (); it.forward ()) {
    *current = it.extract ();
    current++;
  }

  /* Sort the pointer array */
  qsort ((char *) base, count, sizeof (*base), comparator);

  /* Rebuild the list from the sorted pointers */
  current = base;
  for (i = 0; i < count; i++) {
    it.add_to_end (*current);
    current++;
  }
  free(base);
}

// Assuming list has been sorted already, insert new_data to
// keep the list sorted according to the same comparison function.
// Comparision function is the same as used by sort, i.e. uses double
// indirection. Time is O(1) to add to beginning or end.
// Time is linear to add pre-sorted items to an empty list.
// If unique, then don't add duplicate entries.
// Returns true if the element was added to the list.
template <typename T>
bool CLIST<T>::add_sorted(int comparator(const void*, const void*),
                       bool unique, T* new_data) {
  // Check for adding at the end.
  if (last == NULL || comparator(&last->data, &new_data) < 0) {
    CLIST_LINK<T>* new_element = new CLIST_LINK<T>;
    new_element->data = new_data;
    if (last == NULL) {
      new_element->next = new_element;
    } else {
      new_element->next = last->next;
      last->next = new_element;
    }
    last = new_element;
    return true;
  } else if (!unique || last->data != new_data) {
    // Need to use an iterator.
    CLIST_ITERATOR<T> it(this);
    for (it.mark_cycle_pt(); !it.cycled_list(); it.forward()) {
      void* data = it.data();
      if (data == new_data && unique)
        return false;
      if (comparator(&data, &new_data) > 0)
        break;
    }
    if (it.cycled_list())
      it.add_to_end(new_data);
    else
      it.add_before_then_move(new_data);
    return true;
  }
  return false;
}

// Assuming that the minuend and subtrahend are already sorted with
// the same comparison function, shallow clears this and then copies
// the set difference minuend - subtrahend to this, being the elements
// of minuend that do not compare equal to anything in subtrahend.
// If unique is true, any duplicates in minuend are also eliminated.
template <typename T>
void CLIST<T>::set_subtract(int comparator(const void*, const void*),
                         bool unique,
                         CLISType_* minuend, CLISType_* subtrahend) {
  shallow_clear();
  CLIST_ITERATOR<T> m_it(minuend);
  CLIST_ITERATOR<T> s_it(subtrahend);
  // Since both lists are sorted, finding the subtras that are not
  // minus is a case of a parallel iteration.
  for (m_it.mark_cycle_pt(); !m_it.cycled_list(); m_it.forward()) {
    T* minu = m_it.data();
    T* subtra = NULL;
    if (!s_it.empty()) {
      subtra = s_it.data();
      while (!s_it.at_last() &&
             comparator(&subtra, &minu) < 0) {
        s_it.forward();
        subtra = s_it.data();
      }
    }
    if (subtra == NULL || comparator(&subtra, &minu) != 0)
      add_sorted(comparator, unique, minu);
  }
}


/***********************************************************************
 *  MEMBER FUNCTIONS OF CLASS: CLIST_ITERATOR<T>
 *  =========================================
 **********************************************************************/

/***********************************************************************
 *							CLIST_ITERATOR<T>::forward
 *
 *  Move the iterator to the next element of the list.
 *  REMEMBER: ALL LISTS ARE CIRCULAR.
 **********************************************************************/

/***********************************************************************
 *							CLIST_ITERATOR::set_to_list
 *
 *  (Re-)initialise the iterator to point to the start of the list_to_iterate
 *  over.
 **********************************************************************/

template <typename T>
void CLIST_ITERATOR<T>::set_to_list(  //change list
                                        CLIST<T> *list_to_iterate) {
  #ifndef NDEBUG
  if (!this)
    NULL_OBJECT.error ("CLIST_ITERATOR::set_to_list", ABORT, NULL);
  if (!list_to_iterate)
    BAD_PARAMETER.error ("CLIST_ITERATOR::set_to_list", ABORT,
      "list_to_iterate is NULL");
  #endif

  list = list_to_iterate;
  prev = list->last;
  current = list->First ();
  next = current != NULL ? current->next : NULL;
  cycle_pt = NULL;               //await explicit set
  started_cycling = FALSE;
  ex_current_was_last = FALSE;
  ex_current_was_cycle_pt = FALSE;
}


/***********************************************************************
 *							CLIST_ITERATOR::CLIST_ITERATOR
 *
 *  CONSTRUCTOR - set iterator to specified list;
 **********************************************************************/

template <typename T>
CLIST_ITERATOR<T>::CLIST_ITERATOR(CLIST<T> *list_to_iterate) {
  set_to_list(list_to_iterate);
}


/***********************************************************************
 *							CLIST_ITERATOR::add_after_then_move
 *
 *  Add a new element to the list after the current element and move the
 *  iterator to the new element.
 **********************************************************************/

template <typename T>
void CLIST_ITERATOR<T>::add_after_then_move(  // element to add
                                                T *new_data) {
  CLIST_LINK<T> *new_element;

  #ifndef NDEBUG
  if (!this)
    NULL_OBJECT.error ("CLIST_ITERATOR::add_after_then_move", ABORT, NULL);
  if (!list)
    NO_LIST.error ("CLIST_ITERATOR::add_after_then_move", ABORT, NULL);
  if (!new_data)
    BAD_PARAMETER.error ("CLIST_ITERATOR::add_after_then_move", ABORT,
      "new_data is NULL");
  #endif

  new_element = new CLIST_LINK<T>();
  new_element->data = new_data;

  if (list->empty ()) {
    new_element->next = new_element;
    list->last = new_element;
    prev = next = new_element;
  }
  else {
    new_element->next = next;

    if (current) {               //not extracted
      current->next = new_element;
      prev = current;
      if (current == list->last)
        list->last = new_element;
    }
    else {                       //current extracted
      prev->next = new_element;
      if (ex_current_was_last)
        list->last = new_element;
      if (ex_current_was_cycle_pt)
        cycle_pt = new_element;
    }
  }
  current = new_element;
}


/***********************************************************************
 *							CLIST_ITERATOR::add_after_stay_put
 *
 *  Add a new element to the list after the current element but do not move
 *  the iterator to the new element.
 **********************************************************************/

template <typename T>
void CLIST_ITERATOR<T>::add_after_stay_put(  // element to add
                                               T *new_data) {
  CLIST_LINK<T> *new_element;

  #ifndef NDEBUG
  if (!this)
    NULL_OBJECT.error ("CLIST_ITERATOR::add_after_stay_put", ABORT, NULL);
  if (!list)
    NO_LIST.error ("CLIST_ITERATOR::add_after_stay_put", ABORT, NULL);
  if (!new_data)
    BAD_PARAMETER.error ("CLIST_ITERATOR::add_after_stay_put", ABORT,
      "new_data is NULL");
  #endif

  new_element = new CLIST_LINK<T>;
  new_element->data = new_data;

  if (list->empty ()) {
    new_element->next = new_element;
    list->last = new_element;
    prev = next = new_element;
    ex_current_was_last = FALSE;
    current = NULL;
  }
  else {
    new_element->next = next;

    if (current) {               //not extracted
      current->next = new_element;
      if (prev == current)
        prev = new_element;
      if (current == list->last)
        list->last = new_element;
    }
    else {                       //current extracted
      prev->next = new_element;
      if (ex_current_was_last) {
        list->last = new_element;
        ex_current_was_last = FALSE;
      }
    }
    next = new_element;
  }
}


/***********************************************************************
 *							CLIST_ITERATOR::add_before_then_move
 *
 *  Add a new element to the list before the current element and move the
 *  iterator to the new element.
 **********************************************************************/

template <typename T>
void CLIST_ITERATOR<T>::add_before_then_move(  // element to add
                                                 T *new_data) {
  CLIST_LINK<T> *new_element;

  #ifndef NDEBUG
  if (!this)
    NULL_OBJECT.error ("CLIST_ITERATOR::add_before_then_move", ABORT, NULL);
  if (!list)
    NO_LIST.error ("CLIST_ITERATOR::add_before_then_move", ABORT, NULL);
  if (!new_data)
    BAD_PARAMETER.error ("CLIST_ITERATOR::add_before_then_move", ABORT,
      "new_data is NULL");
  #endif

  new_element = new CLIST_LINK<T>;
  new_element->data = new_data;

  if (list->empty ()) {
    new_element->next = new_element;
    list->last = new_element;
    prev = next = new_element;
  }
  else {
    prev->next = new_element;
    if (current) {               //not extracted
      new_element->next = current;
      next = current;
    }
    else {                       //current extracted
      new_element->next = next;
      if (ex_current_was_last)
        list->last = new_element;
      if (ex_current_was_cycle_pt)
        cycle_pt = new_element;
    }
  }
  current = new_element;
}


/***********************************************************************
 *							CLIST_ITERATOR::add_before_stay_put
 *
 *  Add a new element to the list before the current element but dont move the
 *  iterator to the new element.
 **********************************************************************/

template <typename T>
void CLIST_ITERATOR<T>::add_before_stay_put(  // element to add
                                                T *new_data) {
  CLIST_LINK<T> *new_element;

  #ifndef NDEBUG
  if (!this)
    NULL_OBJECT.error ("CLIST_ITERATOR::add_before_stay_put", ABORT, NULL);
  if (!list)
    NO_LIST.error ("CLIST_ITERATOR::add_before_stay_put", ABORT, NULL);
  if (!new_data)
    BAD_PARAMETER.error ("CLIST_ITERATOR::add_before_stay_put", ABORT,
      "new_data is NULL");
  #endif

  new_element = new CLIST_LINK<T>();
  new_element->data = new_data;

  if (list->empty ()) {
    new_element->next = new_element;
    list->last = new_element;
    prev = next = new_element;
    ex_current_was_last = TRUE;
    current = NULL;
  }
  else {
    prev->next = new_element;
    if (current) {               //not extracted
      new_element->next = current;
      if (next == current)
        next = new_element;
    }
    else {                       //current extracted
      new_element->next = next;
      if (ex_current_was_last)
        list->last = new_element;
    }
    prev = new_element;
  }
}


/***********************************************************************
 *							CLIST_ITERATOR::add_list_after
 *
 *  Insert another list to this list after the current element but dont move the
 *  iterator.
 **********************************************************************/

template <typename T>
void CLIST_ITERATOR<T>::add_list_after(CLIST<T> *list_to_add) {
  #ifndef NDEBUG
  if (!this)
    NULL_OBJECT.error ("CLIST_ITERATOR::add_list_after", ABORT, NULL);
  if (!list)
    NO_LIST.error ("CLIST_ITERATOR::add_list_after", ABORT, NULL);
  if (!list_to_add)
    BAD_PARAMETER.error ("CLIST_ITERATOR::add_list_after", ABORT,
      "list_to_add is NULL");
  #endif

  if (!list_to_add->empty ()) {
    if (list->empty ()) {
      list->last = list_to_add->last;
      prev = list->last;
      next = list->First ();
      ex_current_was_last = TRUE;
      current = NULL;
    }
    else {
      if (current) {             //not extracted
        current->next = list_to_add->First ();
        if (current == list->last)
          list->last = list_to_add->last;
        list_to_add->last->next = next;
        next = current->next;
      }
      else {                     //current extracted
        prev->next = list_to_add->First ();
        if (ex_current_was_last) {
          list->last = list_to_add->last;
          ex_current_was_last = FALSE;
        }
        list_to_add->last->next = next;
        next = prev->next;
      }
    }
    list_to_add->last = NULL;
  }
}


/***********************************************************************
 *							CLIST_ITERATOR::add_list_before
 *
 *  Insert another list to this list before the current element. Move the
 *  iterator to the start of the inserted elements
 *  iterator.
 **********************************************************************/

template <typename T>
void CLIST_ITERATOR<T>::add_list_before(CLIST<T> *list_to_add) {
  #ifndef NDEBUG
  if (!this)
    NULL_OBJECT.error ("CLIST_ITERATOR::add_list_before", ABORT, NULL);
  if (!list)
    NO_LIST.error ("CLIST_ITERATOR::add_list_before", ABORT, NULL);
  if (!list_to_add)
    BAD_PARAMETER.error ("CLIST_ITERATOR::add_list_before", ABORT,
      "list_to_add is NULL");
  #endif

  if (!list_to_add->empty ()) {
    if (list->empty ()) {
      list->last = list_to_add->last;
      prev = list->last;
      current = list->First ();
      next = current->next;
      ex_current_was_last = FALSE;
    }
    else {
      prev->next = list_to_add->First ();
      if (current) {             //not extracted
        list_to_add->last->next = current;
      }
      else {                     //current extracted
        list_to_add->last->next = next;
        if (ex_current_was_last)
          list->last = list_to_add->last;
        if (ex_current_was_cycle_pt)
          cycle_pt = prev->next;
      }
      current = prev->next;
      next = current->next;
    }
    list_to_add->last = NULL;
  }
}


/***********************************************************************
 *							CLIST_ITERATOR::extract
 *
 *  Do extraction by removing current from the list, deleting the cons cell
 *  and returning the data to the caller, but NOT updating the iterator.  (So
 *  that any calling loop can do this.)  The iterator's current points to
 *  NULL.  If the data is to be deleted, this is the callers responsibility.
 **********************************************************************/

template <typename T>
T* CLIST_ITERATOR<T>::extract() {
  T *extracted_data;

  #ifndef NDEBUG
  if (!this)
    NULL_OBJECT.error ("CLIST_ITERATOR::extract", ABORT, NULL);
  if (!list)
    NO_LIST.error ("CLIST_ITERATOR::extract", ABORT, NULL);
  if (!current)                  //list empty or
                                 //element extracted
    NULL_CURRENT.error ("CLIST_ITERATOR::extract",
      ABORT, NULL);
  #endif

  if (list->singleton()) {
    // Special case where we do need to change the iterator.
    prev = next = list->last = NULL;
  } else {
    prev->next = next;           //remove from list

    if (current == list->last) {
      list->last = prev;
      ex_current_was_last = TRUE;
    } else {
      ex_current_was_last = FALSE;
    }
  }
  // Always set ex_current_was_cycle_pt so an add/forward will work in a loop.
  ex_current_was_cycle_pt = (current == cycle_pt) ? TRUE : FALSE;
  extracted_data = current->data;
  delete(current);  //destroy CONS cell
  current = NULL;
  return extracted_data;
}


/***********************************************************************
 *							CLIST_ITERATOR::move_to_first()
 *
 *  Move current so that it is set to the start of the list.
 *  Return data just in case anyone wants it.
 **********************************************************************/

template <typename T>
T* CLIST_ITERATOR<T>::move_to_first() {
  #ifndef NDEBUG
  if (!this)
    NULL_OBJECT.error ("CLIST_ITERATOR::move_to_first", ABORT, NULL);
  if (!list)
    NO_LIST.error ("CLIST_ITERATOR::move_to_first", ABORT, NULL);
  #endif

  current = list->First ();
  prev = list->last;
  next = current != NULL ? current->next : NULL;
  return current != NULL ? current->data : NULL;
}


/***********************************************************************
 *							CLIST_ITERATOR::mark_cycle_pt()
 *
 *  Remember the current location so that we can tell whether we've returned
 *  to this point later.
 *
 *  If the current point is deleted either now, or in the future, the cycle
 *  point will be set to the next item which is set to current.  This could be
 *  by a forward, add_after_then_move or add_after_then_move.
 **********************************************************************/

template <typename T>
void CLIST_ITERATOR<T>::mark_cycle_pt() {
  #ifndef NDEBUG
  if (!this)
    NULL_OBJECT.error ("CLIST_ITERATOR::mark_cycle_pt", ABORT, NULL);
  if (!list)
    NO_LIST.error ("CLIST_ITERATOR::mark_cycle_pt", ABORT, NULL);
  #endif

  if (current)
    cycle_pt = current;
  else
    ex_current_was_cycle_pt = TRUE;
  started_cycling = FALSE;
}


/***********************************************************************
 *							CLIST_ITERATOR::at_first()
 *
 *  Are we at the start of the list?
 *
 **********************************************************************/

template <typename T>
BOOL8 CLIST_ITERATOR<T>::at_first() {
  #ifndef NDEBUG
  if (!this)
    NULL_OBJECT.error ("CLIST_ITERATOR::at_first", ABORT, NULL);
  if (!list)
    NO_LIST.error ("CLIST_ITERATOR::at_first", ABORT, NULL);
  #endif

                                 //we're at a deleted
  return ((list->empty ()) || (current == list->First ()) || ((current == NULL) &&
    (prev == list->last) &&      //NON-last pt between
    !ex_current_was_last));      //first and last
}


/***********************************************************************
 *							CLIST_ITERATOR::at_last()
 *
 *  Are we at the end of the list?
 *
 **********************************************************************/

template <typename T>
BOOL8 CLIST_ITERATOR<T>::at_last() {
  #ifndef NDEBUG
  if (!this)
    NULL_OBJECT.error ("CLIST_ITERATOR::at_last", ABORT, NULL);
  if (!list)
    NO_LIST.error ("CLIST_ITERATOR::at_last", ABORT, NULL);
  #endif

                                 //we're at a deleted
  return ((list->empty ()) || (current == list->last) || ((current == NULL) &&
    (prev == list->last) &&      //last point between
    ex_current_was_last));       //first and last
}


/***********************************************************************
 *							CLIST_ITERATOR::cycled_list()
 *
 *  Have we returned to the cycle_pt since it was set?
 *
 **********************************************************************/

template <typename T>
BOOL8 CLIST_ITERATOR<T>::cycled_list() {
  #ifndef NDEBUG
  if (!this)
    NULL_OBJECT.error ("CLIST_ITERATOR::cycled_list", ABORT, NULL);
  if (!list)
    NO_LIST.error ("CLIST_ITERATOR::cycled_list", ABORT, NULL);
  #endif

  return ((list->empty ()) || ((current == cycle_pt) && started_cycling));

}


/***********************************************************************
 *							CLIST_ITERATOR::length()
 *
 *  Return the length of the list
 *
 **********************************************************************/

template <typename T>
inT32 CLIST_ITERATOR<T>::length() {
  #ifndef NDEBUG
  if (!this)
    NULL_OBJECT.error ("CLIST_ITERATOR::length", ABORT, NULL);
  if (!list)
    NO_LIST.error ("CLIST_ITERATOR::length", ABORT, NULL);
  #endif

  return list->length ();
}


/***********************************************************************
 *							CLIST_ITERATOR::sort()
 *
 *  Sort the elements of the list, then reposition at the start.
 *
 **********************************************************************/

template <typename T>
void CLIST_ITERATOR<T>::sort (           //sort elements
int comparator (                 //comparison routine
const void *, const void *)) {
  #ifndef NDEBUG
  if (!this)
    NULL_OBJECT.error ("CLIST_ITERATOR::sort", ABORT, NULL);
  if (!list)
    NO_LIST.error ("CLIST_ITERATOR::sort", ABORT, NULL);
  #endif

  list->sort (comparator);
  move_to_first();
}


/***********************************************************************
 *							CLIST_ITERATOR::add_to_end
 *
 *  Add a new element to the end of the list without moving the iterator.
 *  This is provided because a single linked list cannot move to the last as
 *  the iterator couldn't set its prev pointer.  Adding to the end is
 *  essential for implementing
              queues.
**********************************************************************/

template <typename T>
void CLIST_ITERATOR<T>::add_to_end(  // element to add
                                       T *new_data) {
  CLIST_LINK<T> *new_element;

  #ifndef NDEBUG
  if (!this)
    NULL_OBJECT.error ("CLIST_ITERATOR::add_to_end", ABORT, NULL);
  if (!list)
    NO_LIST.error ("CLIST_ITERATOR::add_to_end", ABORT, NULL);
  if (!new_data)
    BAD_PARAMETER.error ("CLIST_ITERATOR::add_to_end", ABORT,
      "new_data is NULL");
  #endif

  if (this->at_last ()) {
    this->add_after_stay_put (new_data);
  }
  else {
    if (this->at_first ()) {
      this->add_before_stay_put (new_data);
      list->last = prev;
    }
    else {                       //Iteratr is elsewhere
      new_element = new CLIST_LINK<T>;
      new_element->data = new_data;

      new_element->next = list->last->next;
      list->last->next = new_element;
      list->last = new_element;
    }
  }
}

template <typename T>
T *CLIST_ITERATOR<T>::forward() {
  #ifndef NDEBUG
  if (!this)
    NULL_OBJECT.error ("CLIST_ITERATOR<T>::forward", ABORT, NULL);
  if (!list)
    NO_LIST.error ("CLIST_ITERATOR<T>::forward", ABORT, NULL);
  #endif
  if (list->empty ())
    return NULL;

  if (current) {                 //not removed so
                                 //set previous
    prev = current;
    started_cycling = TRUE;
    // In case next is deleted by another iterator, get next from current.
    current = current->next;
  } else {
    if (ex_current_was_cycle_pt)
      cycle_pt = next;
    current = next;
  }
  next = current->next;

  #ifndef NDEBUG
  if (!current)
    NULL_DATA.error ("CLIST_ITERATOR<T>::forward", ABORT, NULL);
  if (!next)
    NULL_NEXT.error ("CLIST_ITERATOR<T>::forward", ABORT,
                     "This is: %p  Current is: %p", this, current);
  #endif
  return current->data;
}


/***********************************************************************
 *							CLIST_ITERATOR<T>::data_relative
 *
 *  Return the data pointer to the element "offset" elements from current.
 *  "offset" must not be less than -1.
 *  (This function can't be INLINEd because it contains a loop)
 **********************************************************************/

template <typename T>
T *CLIST_ITERATOR<T>::data_relative(                //get data + or - ...
                                    inT8 offset) {  //offset from current
  CLIST_LINK<T> *ptr;

  #ifndef NDEBUG
  if (!this)
    NULL_OBJECT.error ("CLIST_ITERATOR<T>::data_relative", ABORT, NULL);
  if (!list)
    NO_LIST.error ("CLIST_ITERATOR<T>::data_relative", ABORT, NULL);
  if (list->empty ())
    EMPTY_LIST.error ("CLIST_ITERATOR<T>::data_relative", ABORT, NULL);
  if (offset < -1)
    BAD_PARAMETER.error ("CLIST_ITERATOR<T>::data_relative", ABORT,
      "offset < -l");
  #endif

  if (offset == -1)
    ptr = prev;
  else
    for (ptr = current ? current : prev; offset-- > 0; ptr = ptr->next);

  #ifndef NDEBUG
  if (!ptr)
    NULL_DATA.error ("CLIST_ITERATOR<T>::data_relative", ABORT, NULL);
  #endif

  return ptr->data;
}


/***********************************************************************
 *							CLIST_ITERATOR<T>::move_to_last()
 *
 *  Move current so that it is set to the end of the list.
 *  Return data just in case anyone wants it.
 *  (This function can't be INLINEd because it contains a loop)
 **********************************************************************/

template <typename T>
T *CLIST_ITERATOR<T>::move_to_last() {
  #ifndef NDEBUG
  if (!this)
    NULL_OBJECT.error ("CLIST_ITERATOR<T>::move_to_last", ABORT, NULL);
  if (!list)
    NO_LIST.error ("CLIST_ITERATOR<T>::move_to_last", ABORT, NULL);
  #endif

  while (current != list->last)
    forward();

  if (current == NULL)
    return NULL;
  else
    return current->data;
}


/***********************************************************************
 *							CLIST_ITERATOR<T>::exchange()
 *
 *  Given another iterator, whose current element is a different element on
 *  the same list list OR an element of another list, exchange the two current
 *  elements.  On return, each iterator points to the element which was the
 *  other iterators current on entry.
 *  (This function hasn't been in-lined because its a bit big!)
 **********************************************************************/

template <typename T>
void CLIST_ITERATOR<T>::exchange(                             //positions of 2 links
                              CLIST_IT_Type_ *other_it) {  //other iterator
  const ERRCODE DONT_EXCHANGE_DELETED =
    "Can't exchange deleted elements of lists";

  CLIST_LINK<T> *old_current;

  #ifndef NDEBUG
  if (!this)
    NULL_OBJECT.error ("CLIST_ITERATOR<T>::exchange", ABORT, NULL);
  if (!list)
    NO_LIST.error ("CLIST_ITERATOR<T>::exchange", ABORT, NULL);
  if (!other_it)
    BAD_PARAMETER.error ("CLIST_ITERATOR<T>::exchange", ABORT, "other_it NULL");
  if (!(other_it->list))
    NO_LIST.error ("CLIST_ITERATOR<T>::exchange", ABORT, "other_it");
  #endif

  /* Do nothing if either list is empty or if both iterators reference the same
  link */

  if ((list->empty ()) ||
    (other_it->list->empty ()) || (current == other_it->current))
    return;

  /* Error if either current element is deleted */

  if (!current || !other_it->current)
    DONT_EXCHANGE_DELETED.error ("CLIST_ITERATOR<T>.exchange", ABORT, NULL);

  /* Now handle the 4 cases: doubleton list; non-doubleton adjacent elements
  (other before this); non-doubleton adjacent elements (this before other);
  non-adjacent elements. */

                                 //adjacent links
  if ((next == other_it->current) ||
  (other_it->next == current)) {
                                 //doubleton list
    if ((next == other_it->current) &&
    (other_it->next == current)) {
      prev = next = current;
      other_it->prev = other_it->next = other_it->current;
    }
    else {                       //non-doubleton with
                                 //adjacent links
                                 //other before this
      if (other_it->next == current) {
        other_it->prev->next = current;
        other_it->current->next = next;
        current->next = other_it->current;
        other_it->next = other_it->current;
        prev = current;
      }
      else {                     //this before other
        prev->next = other_it->current;
        current->next = other_it->next;
        other_it->current->next = current;
        next = current;
        other_it->prev = other_it->current;
      }
    }
  }
  else {                         //no overlap
    prev->next = other_it->current;
    current->next = other_it->next;
    other_it->prev->next = current;
    other_it->current->next = next;
  }

  /* update end of list pointer when necessary (remember that the 2 iterators
    may iterate over different lists!) */

  if (list->last == current)
    list->last = other_it->current;
  if (other_it->list->last == other_it->current)
    other_it->list->last = current;

  if (current == cycle_pt)
    cycle_pt = other_it->cycle_pt;
  if (other_it->current == other_it->cycle_pt)
    other_it->cycle_pt = cycle_pt;

  /* The actual exchange - in all cases*/

  old_current = current;
  current = other_it->current;
  other_it->current = old_current;
}


/***********************************************************************
 *							CLIST_ITERATOR<T>::extract_sublist()
 *
 *  This is a private member, used only by CLIST<T>::assign_to_sublist.
 *  Given another iterator for the same list, extract the links from THIS to
 *  OTHER inclusive, link them into a new circular list, and return a
 *  pointer to the last element.
 *  (Can't inline this function because it contains a loop)
 **********************************************************************/

template <typename T>
CLIST_LINK<T> *CLIST_ITERATOR<T>::extract_sublist(                             //from this current
                                            CLIST_IT_Type_ *other_it) {  //to other current
  CLIST_IT_Type_ temp_it = *this;
  CLIST_LINK<T> *end_of_new_list;

  const ERRCODE BAD_SUBLIST = "Can't find sublist end point in original list";
  #ifndef NDEBUG
  const ERRCODE BAD_EXTRACTION_PTS =
    "Can't extract sublist from points on different lists";
  const ERRCODE DONT_EXTRACT_DELETED =
    "Can't extract a sublist marked by deleted points";

  if (!this)
    NULL_OBJECT.error ("CLIST_ITERATOR<T>::extract_sublist", ABORT, NULL);
  if (!other_it)
    BAD_PARAMETER.error ("CLIST_ITERATOR<T>::extract_sublist", ABORT,
      "other_it NULL");
  if (!list)
    NO_LIST.error ("CLIST_ITERATOR<T>::extract_sublist", ABORT, NULL);
  if (list != other_it->list)
    BAD_EXTRACTION_PTS.error ("CLIST_ITERATOR<T>.extract_sublist", ABORT, NULL);
  if (list->empty ())
    EMPTY_LIST.error ("CLIST_ITERATOR<T>::extract_sublist", ABORT, NULL);

  if (!current || !other_it->current)
    DONT_EXTRACT_DELETED.error ("CLIST_ITERATOR<T>.extract_sublist", ABORT,
      NULL);
  #endif

  ex_current_was_last = other_it->ex_current_was_last = FALSE;
  ex_current_was_cycle_pt = FALSE;
  other_it->ex_current_was_cycle_pt = FALSE;

  temp_it.mark_cycle_pt ();
  do {                           //walk sublist
    if (temp_it.cycled_list ())  //cant find end pt
      BAD_SUBLIST.error ("CLIST_ITERATOR<T>.extract_sublist", ABORT, NULL);

    if (temp_it.at_last ()) {
      list->last = prev;
      ex_current_was_last = other_it->ex_current_was_last = TRUE;
    }

    if (temp_it.current == cycle_pt)
      ex_current_was_cycle_pt = TRUE;

    if (temp_it.current == other_it->cycle_pt)
      other_it->ex_current_was_cycle_pt = TRUE;

    temp_it.forward ();
  }
  while (temp_it.prev != other_it->current);

                                 //circularise sublist
  other_it->current->next = current;
  end_of_new_list = other_it->current;

                                 //sublist = whole list
  if (prev == other_it->current) {
    list->last = NULL;
    prev = current = next = NULL;
    other_it->prev = other_it->current = other_it->next = NULL;
  }
  else {
    prev->next = other_it->next;
    current = other_it->current = NULL;
    next = other_it->next;
    other_it->prev = prev;
  }
  return end_of_new_list;
}

#define CLISTIZE(CLASSNAME)\
  typedef CLIST<CLASSNAME> CLASSNAME##_CLIST;\
  typedef CLIST_ITERATOR<CLASSNAME> CLASSNAME##_C_IT;\

#endif
