/**********************************************************************
 * File:        strngs.h  (Formerly strings.h)
 * Description: STRING class definition.
 * Author:          Ray Smith
 * Created:          Fri Feb 15 09:15:01 GMT 1991
 *
 * (C) Copyright 1991, Hewlett-Packard Ltd.
 ** Licensed under the Apache License, Version 2.0 (the "License");
 ** you may not use this file except in compliance with the License.
 ** You may obtain a copy of the License at
 ** http://www.apache.org/licenses/LICENSE-2.0
 ** Unless required by applicable law or agreed to in writing, software
 ** distributed under the License is distributed on an "AS IS" BASIS,
 ** WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 ** See the License for the specific language governing permissions and
 ** limitations under the License.
 *
 **********************************************************************/

#ifndef           STRNGS_H
#define           STRNGS_H

#include "serialis.h"

#include <string>

#ifdef CCUTIL_EXPORTS
#define CCUTIL_API __declspec(dllexport)
#elif defined(CCUTIL_IMPORTS)
#define CCUTIL_API __declspec(dllimport)
#else
#define CCUTIL_API
#endif

class CCUTIL_API STRING : public std::string
{
public:
  STRING() : std::string() {}
  STRING(const std::string &string) : std::string(string) {}
  STRING(const STRING &string) : std::string(string) {}
  STRING(const char *string) : std::string(string == NULL ? "" : string) {}

  bool contains(const char c) const { return find(c) != npos; }
  const char *string() const { return c_str(); }

#ifdef STRING_IS_PROTECTED
  // len is number of chars in s to insert starting at index in this string
  void insert_range(inT32 index, const char*s, int len) { insert(index, s, len); }
  void erase_range(inT32 index, int len) { erase(index, len); }
  void truncate_at(inT32 index) { erase(index); }
#endif

  bool operator!= (const char *string) const { return *this == string; }

  // Appends the given string and int (as a %d) to this.
  // += cannot be used for ints as there as a char += operator that would
  // be ambiguous, and ints usually need a string before or between them
  // anyway.
  void add_str_int(const char* str, int number);
};

#endif

