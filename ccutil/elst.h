/**********************************************************************
* File:        elst.h  (Formerly elist.h)
* Description: Embedded list module include file.
* Author:      Phil Cheatle
* Created:     Mon Jan 07 08:35:34 GMT 1991
*
* (C) Copyright 1991, Hewlett-Packard Ltd.
** Licensed under the Apache License, Version 2.0 (the "License");
** you may not use this file except in compliance with the License.
** You may obtain a copy of the License at
** http://www.apache.org/licenses/LICENSE-2.0
** Unless required by applicable law or agreed to in writing, software
** distributed under the License is distributed on an "AS IS" BASIS,
** WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
** See the License for the specific language governing permissions and
** limitations under the License.
*
**********************************************************************/

#ifndef ELST_H
#define ELST_H

#include <stdio.h>
#include "host.h"
#include "serialis.h"
#include "lsterr.h"

/**********************************************************************
This module implements list classes and iterators.
The following list types and iterators are provided:

List type        List Class      Iterator Class     Element Class
---------         ----------      --------------      -------------

Embedded list       ELIST
ELIST_ITERATOR
ELIST_LINK
(Single linked)

Embedded list       ELIST2
ELIST2_ITERATOR
ELIST2_LINK
(Double linked)

Cons List           CLIST
CLIST_ITERATOR
CLIST_LINK
(Single linked)

Cons List           CLIST2
CLIST2_ITERATOR
CLIST2_LINK
(Double linked)

An embedded list is where the list pointers are provided by a generic class.
Data types to be listed inherit from the generic class.  Data is thus linked
in only ONE list at any one time.

A cons list has a separate structure for a "cons cell".  This contains the
list pointer(s) AND a pointer to the data structure held on the list.  A
structure can be on many cons lists at the same time, and the structure does
not need to inherit from any generic class in order to be on the list.

The implementation of lists is very careful about space and speed overheads.
This is why many embedded lists are provided. The same concerns mean that
in-line type coercion is done, rather than use virtual functions.  This is
cumbersome in that each data type to be listed requires its own iterator and
list class - though macros can gererate these.  It also prevents heterogenous
lists.
**********************************************************************/

/**********************************************************************
*                          CLASS - ELIST_LINK
*
*                          Generic link class for singly linked lists with embedded links
*
*  Note:  No destructor - elements are assumed to be destroyed EITHER after
*  they have been extracted from a list OR by the ELIST destructor which
*  walks the list.
**********************************************************************/

template<typename T>
class ELIST_ITERATOR;

template <typename T>
class DLLSYM ELIST_LINK
{
  typedef ELIST_LINK<T> ELIST_LINK_Type_;
  template <typename U>
  friend class ELIST_ITERATOR;
  template <typename V>
  friend class ELIST;

  T *next;

public:
    ELIST_LINK() : next(NULL){
  }
  //constructor

  ELIST_LINK(                       //copy constructor
      const ELIST_LINK_Type_ &) : next(NULL) {  //dont copy link
  }

  void operator= (             //dont copy links
    const ELIST_LINK_Type_ &) {
      next = NULL;
  }
};

/**********************************************************************
* CLASS - ELIST
*
* Generic list class for singly linked lists with embedded links
**********************************************************************/

template <typename T>
class DLLSYM ELIST
{
  typedef ELIST<T> ELIST_Type_;
  template<typename U>
  friend class ELIST_ITERATOR;

  T *last;              //End of list
  //(Points to head)
  T *First() {  // return first
    return last ? last->next : NULL;
  }

  ELIST(const ELIST_Type_&);  // prohibited
  ELIST& operator=(const ELIST_Type_&); //prohibited

public:
  typedef ELIST_ITERATOR<T> iterator;

  ELIST() : last(NULL) {  //constructor
  }

  void clear ()
  {
    T *ptr;
    T *next;

#ifndef NDEBUG
    if (!this)
      NULL_OBJECT.error ("ELIST::clear", ABORT, NULL);
#endif
    if (!empty ()) {
      ptr = last->next;            //set to first
      last->next = NULL;           //break circle
      last = NULL;                 //set list empty
      while (ptr) {
        next = ptr->next;
        delete reinterpret_cast<T*>(ptr);
        ptr = next;
      }
    }
  }

  bool empty() const {  //is list empty?
    return !last;
  }

  bool singleton() const {
    return last ? (last == last->next) : false;
  }

  void shallow_copy(                     //dangerous!!
    ELIST_Type_ *from_list) {  //beware destructors!!
      last = from_list->last;
  }

  void deep_copy(const ELIST_Type_* src_list, T* (*copier)(const T*)){
    ELIST_ITERATOR<T> from_it(const_cast<ELIST_Type_*>(src_list));
    ELIST_ITERATOR<T> to_it(this); 
    for (from_it.mark_cycle_pt(); !from_it.cycled_list(); from_it.forward())
      to_it.add_after_then_move((*copier)(from_it.data()));
  }

  void assign_to_sublist(                           //to this list
    ELIST_ITERATOR<T> *start_it,  //from list start
    ELIST_ITERATOR<T> *end_it){   //from list end
      const ERRCODE LIST_NOT_EMPTY =
        "Destination list must be empty before extracting a sublist";

#ifndef NDEBUG
      if (!this)
        NULL_OBJECT.error ("ELIST::assign_to_sublist", ABORT, NULL);
#endif

      if (!empty ())
        LIST_NOT_EMPTY.error ("ELIST.assign_to_sublist", ABORT, NULL);

      last = start_it->extract_sublist (end_it);
  }

  inT32 length() const{  // # elements in list
    ELIST_ITERATOR<T> it(const_cast< ELIST_Type_* >(this));
    inT32 count = 0;

#ifndef NDEBUG
    if (!this)
      NULL_OBJECT.error ("ELIST::length", ABORT, NULL);
#endif

    for (it.mark_cycle_pt (); !it.cycled_list (); it.forward ())
      count++;
    return count;
  }

  void sort (                  //sort elements
    int comparator (           //comparison routine
    const void *, const void *)){
      ELIST_ITERATOR<T> it(this);
      inT32 count;
      T **base;             //ptr array to sort
      T **current;
      inT32 i;

#ifndef NDEBUG
      if (!this)
        NULL_OBJECT.error ("ELIST::sort", ABORT, NULL);
#endif

      /* Allocate an array of pointers, one per list element */
      count = length ();
      base = (T **) malloc (count * sizeof (T *));

      /* Extract all elements, putting the pointers in the array */
      current = base;
      for (it.mark_cycle_pt (); !it.cycled_list (); it.forward ()) {
        *current = it.extract ();
        current++;
      }

      /* Sort the pointer array */
      qsort ((char *) base, count, sizeof (*base), comparator);

      /* Rebuild the list from the sorted pointers */
      current = base;
      for (i = 0; i < count; i++) {
        it.add_to_end (*current);
        current++;
      }
      free(base);
  }

  // Assuming list has been sorted already, insert new_link to
  // keep the list sorted according to the same comparison function.
  // Comparision function is the same as used by sort, i.e. uses double
  // indirection. Time is O(1) to add to beginning or end.
  // Time is linear to add pre-sorted items to an empty list.
  // If unique is set to true and comparator() returns 0 (an entry with the
  // same information as the one contained in new_link is already in the
  // list) - new_link is not added to the list and the function returns the
  // pointer to the identical entry that already exists in the list
  // (otherwise the function returns new_link).
  T *add_sorted_and_find(int comparator(const void*, const void*),
    bool unique, T* new_link){
      // Check for adding at the end.
      if (last == NULL || comparator(&last, &new_link) < 0) {
        if (last == NULL) {
          new_link->next = new_link;
        } else {
          new_link->next = last->next;
          last->next = new_link;
        }
        last = new_link;
      } else {
        // Need to use an iterator.
        ELIST_ITERATOR<T> it(this);
        for (it.mark_cycle_pt(); !it.cycled_list(); it.forward()) {
          T* link = it.data();
          int compare = comparator(&link, &new_link);
          if (compare > 0) {
            break;
          } else if (unique && compare == 0) {
            return link;
          }
        }
        if (it.cycled_list())
          it.add_to_end(new_link);
        else
          it.add_before_then_move(new_link);
      }
      return new_link;
  }

  // Same as above, but returns true if the new entry was inserted, false
  // if the identical entry already existed in the list.
  bool add_sorted(int comparator(const void*, const void*),
    bool unique, T* new_link) {
      return (add_sorted_and_find(comparator, unique, new_link) == new_link);
  }

};

/***********************************************************************
*                          CLASS - ELIST_ITERATOR
*
*                          Generic iterator class for singly linked lists with embedded links
**********************************************************************/

template <typename T>
class DLLSYM ELIST_ITERATOR
{
  typedef ELIST_ITERATOR<T> ELIST_IT_Type_;
//   template <typename U>
//   friend void ELIST<U>::assign_to_sublist(ELIST_ITERATOR<U> *, ELIST_ITERATOR<U> *);
  template <typename U> friend class ELIST;

  ELIST<T> *list;              //List being iterated
  T *prev;              //prev element
  T *current;           //current element
  T *next;              //next element
  bool ex_current_was_last;      //current extracted
  //was end of list
  bool ex_current_was_cycle_pt; //current extracted
  //was cycle point
  T *cycle_pt;          //point we are cycling
  //the list to.
  bool started_cycling;         //Have we moved off
  //the start?

  T *extract_sublist(                            //from this current...
    ELIST_IT_Type_ *other_it);  //to other current

public:
    ELIST_ITERATOR() : list(NULL) {  //constructor
  }                            //unassigned list

  ELIST_ITERATOR(  //constructor
    ELIST<T> *list_to_iterate){
      set_to_list(list_to_iterate);
  }

  void set_to_list(  //change list
    ELIST<T> *list_to_iterate);

  void add_after_then_move(                        //add after current &
    T *new_link);  //move to new

  void add_after_stay_put(                        //add after current &
    T *new_link);  //stay at current

  void add_before_then_move(                        //add before current &
    T *new_link);  //move to new

  void add_before_stay_put(                        //add before current &
    T *new_link);  //stay at current

  void add_list_after(                      //add a list &
    ELIST<T> *list_to_add);  //stay at current

  void add_list_before(                      //add a list &
    ELIST<T> *list_to_add);  //move to it 1st item

  T *data() {  //get current data
#ifndef NDEBUG
    if (!list)
      NO_LIST.error ("ELIST_ITERATOR::data", ABORT, NULL);
    if (!current)
      NULL_DATA.error ("ELIST_ITERATOR::data", ABORT, NULL);
#endif
    return current;
  }

  T *data_relative(               //get data + or - ...
    inT8 offset);  //offset from current

  T *forward();  //move to next element

  T *extract();  //remove from list

  T *move_to_first();  //go to start of list

  T *move_to_last();  //go to end of list

  void mark_cycle_pt();  //remember current

  bool empty() {  //is list empty?
#ifndef NDEBUG
    if (!list)
      NO_LIST.error ("ELIST_ITERATOR::empty", ABORT, NULL);
#endif
    return list->empty ();
  }

  bool current_extracted() {  //current extracted?
    return !current;
  }

  bool at_first();  //Current is first?

  bool at_last();  //Current is last?

  bool cycled_list();  //Completed a cycle?

  void add_to_end(                        //add at end &
    T *new_link);  //dont move

  void exchange(                            //positions of 2 links
    ELIST_IT_Type_ *other_it);  //other iterator

  inT32 length();  //# elements in list

  void sort (                  //sort elements
    int comparator (           //comparison routine
    const void *, const void *));

};

/***********************************************************************
*                          ELIST_ITERATOR::set_to_list
*
*  (Re-)initialise the iterator to point to the start of the list_to_iterate
*  over.
**********************************************************************/

template<typename T>
void ELIST_ITERATOR<T>::set_to_list(  //change list
                                        ELIST<T> *list_to_iterate) {
#ifndef NDEBUG
                                          if (!this)
                                            NULL_OBJECT.error ("ELIST_ITERATOR::set_to_list", ABORT, NULL);
                                          if (!list_to_iterate)
                                            BAD_PARAMETER.error ("ELIST_ITERATOR::set_to_list", ABORT,
                                            "list_to_iterate is NULL");
#endif

                                          list = list_to_iterate;
                                          prev = list->last;
                                          current = list->First ();
                                          next = current ? current->next : NULL;
                                          cycle_pt = NULL;               //await explicit set
                                          started_cycling = FALSE;
                                          ex_current_was_last = FALSE;
                                          ex_current_was_cycle_pt = FALSE;
}

/***********************************************************************
*                          ELIST_ITERATOR::add_after_then_move
*
*  Add a new element to the list after the current element and move the
*  iterator to the new element.
**********************************************************************/

template <typename T>
void ELIST_ITERATOR<T>::add_after_then_move(  // element to add
                                                T *new_element) {
#ifndef NDEBUG
                                                  if (!this)
                                                    NULL_OBJECT.error ("ELIST_ITERATOR::add_after_then_move", ABORT, NULL);
                                                  if (!list)
                                                    NO_LIST.error ("ELIST_ITERATOR::add_after_then_move", ABORT, NULL);
                                                  if (!new_element)
                                                    BAD_PARAMETER.error ("ELIST_ITERATOR::add_after_then_move", ABORT,
                                                    "new_element is NULL");
                                                  if (new_element->next)
                                                    STILL_LINKED.error ("ELIST_ITERATOR::add_after_then_move", ABORT, NULL);
#endif

                                                  if (list->empty ()) {
                                                    new_element->next = new_element;
                                                    list->last = new_element;
                                                    prev = next = new_element;
                                                  }
                                                  else {
                                                    new_element->next = next;

                                                    if (current) {               //not extracted
                                                      current->next = new_element;
                                                      prev = current;
                                                      if (current == list->last)
                                                        list->last = new_element;
                                                    }
                                                    else {                       //current extracted
                                                      prev->next = new_element;
                                                      if (ex_current_was_last)
                                                        list->last = new_element;
                                                      if (ex_current_was_cycle_pt)
                                                        cycle_pt = new_element;
                                                    }
                                                  }
                                                  current = new_element;
}


/***********************************************************************
*                          ELIST_ITERATOR::add_after_stay_put
*
*  Add a new element to the list after the current element but do not move
*  the iterator to the new element.
**********************************************************************/

template <typename T>
void ELIST_ITERATOR<T>::add_after_stay_put(  // element to add
                                               T *new_element) {
#ifndef NDEBUG
                                                 if (!this)
                                                   NULL_OBJECT.error ("ELIST_ITERATOR::add_after_stay_put", ABORT, NULL);
                                                 if (!list)
                                                   NO_LIST.error ("ELIST_ITERATOR::add_after_stay_put", ABORT, NULL);
                                                 if (!new_element)
                                                   BAD_PARAMETER.error ("ELIST_ITERATOR::add_after_stay_put", ABORT,
                                                   "new_element is NULL");
                                                 if (new_element->next)
                                                   STILL_LINKED.error ("ELIST_ITERATOR::add_after_stay_put", ABORT, NULL);
#endif

                                                 if (list->empty ()) {
                                                   new_element->next = new_element;
                                                   list->last = new_element;
                                                   prev = next = new_element;
                                                   ex_current_was_last = FALSE;
                                                   current = NULL;
                                                 }
                                                 else {
                                                   new_element->next = next;

                                                   if (current) {               //not extracted
                                                     current->next = new_element;
                                                     if (prev == current)
                                                       prev = new_element;
                                                     if (current == list->last)
                                                       list->last = new_element;
                                                   }
                                                   else {                       //current extracted
                                                     prev->next = new_element;
                                                     if (ex_current_was_last) {
                                                       list->last = new_element;
                                                       ex_current_was_last = FALSE;
                                                     }
                                                   }
                                                   next = new_element;
                                                 }
}


/***********************************************************************
*                          ELIST_ITERATOR::add_before_then_move
*
*  Add a new element to the list before the current element and move the
*  iterator to the new element.
**********************************************************************/

template <typename T>
void ELIST_ITERATOR<T>::add_before_then_move(  // element to add
                                                 T *new_element) {
#ifndef NDEBUG
                                                   if (!this)
                                                     NULL_OBJECT.error ("ELIST_ITERATOR::add_before_then_move", ABORT, NULL);
                                                   if (!list)
                                                     NO_LIST.error ("ELIST_ITERATOR::add_before_then_move", ABORT, NULL);
                                                   if (!new_element)
                                                     BAD_PARAMETER.error ("ELIST_ITERATOR::add_before_then_move", ABORT,
                                                     "new_element is NULL");
                                                   if (new_element->next)
                                                     STILL_LINKED.error ("ELIST_ITERATOR::add_before_then_move", ABORT, NULL);
#endif

                                                   if (list->empty ()) {
                                                     new_element->next = new_element;
                                                     list->last = new_element;
                                                     prev = next = new_element;
                                                   }
                                                   else {
                                                     prev->next = new_element;
                                                     if (current) {               //not extracted
                                                       new_element->next = current;
                                                       next = current;
                                                     }
                                                     else {                       //current extracted
                                                       new_element->next = next;
                                                       if (ex_current_was_last)
                                                         list->last = new_element;
                                                       if (ex_current_was_cycle_pt)
                                                         cycle_pt = new_element;
                                                     }
                                                   }
                                                   current = new_element;
}


/***********************************************************************
*                          ELIST_ITERATOR::add_before_stay_put
*
*  Add a new element to the list before the current element but dont move the
*  iterator to the new element.
**********************************************************************/

template <typename T>
void ELIST_ITERATOR<T>::add_before_stay_put(  // element to add
                                                T *new_element) {
#ifndef NDEBUG
                                                  if (!this)
                                                    NULL_OBJECT.error ("ELIST_ITERATOR::add_before_stay_put", ABORT, NULL);
                                                  if (!list)
                                                    NO_LIST.error ("ELIST_ITERATOR::add_before_stay_put", ABORT, NULL);
                                                  if (!new_element)
                                                    BAD_PARAMETER.error ("ELIST_ITERATOR::add_before_stay_put", ABORT,
                                                    "new_element is NULL");
                                                  if (new_element->next)
                                                    STILL_LINKED.error ("ELIST_ITERATOR::add_before_stay_put", ABORT, NULL);
#endif

                                                  if (list->empty ()) {
                                                    new_element->next = new_element;
                                                    list->last = new_element;
                                                    prev = next = new_element;
                                                    ex_current_was_last = TRUE;
                                                    current = NULL;
                                                  }
                                                  else {
                                                    prev->next = new_element;
                                                    if (current) {               //not extracted
                                                      new_element->next = current;
                                                      if (next == current)
                                                        next = new_element;
                                                    }
                                                    else {                       //current extracted
                                                      new_element->next = next;
                                                      if (ex_current_was_last)
                                                        list->last = new_element;
                                                    }
                                                    prev = new_element;
                                                  }
}


/***********************************************************************
*                          ELIST_ITERATOR::add_list_after
*
*  Insert another list to this list after the current element but dont move the
*  iterator.
**********************************************************************/

template <typename T>
void ELIST_ITERATOR<T>::add_list_after(ELIST<T> *list_to_add) {
#ifndef NDEBUG
  if (!this)
    NULL_OBJECT.error ("ELIST_ITERATOR::add_list_after", ABORT, NULL);
  if (!list)
    NO_LIST.error ("ELIST_ITERATOR::add_list_after", ABORT, NULL);
  if (!list_to_add)
    BAD_PARAMETER.error ("ELIST_ITERATOR::add_list_after", ABORT,
    "list_to_add is NULL");
#endif

  if (!list_to_add->empty ()) {
    if (list->empty ()) {
      list->last = list_to_add->last;
      prev = list->last;
      next = list->First ();
      ex_current_was_last = TRUE;
      current = NULL;
    }
    else {
      if (current) {             //not extracted
        current->next = list_to_add->First ();
        if (current == list->last)
          list->last = list_to_add->last;
        list_to_add->last->next = next;
        next = current->next;
      }
      else {                     //current extracted
        prev->next = list_to_add->First ();
        if (ex_current_was_last) {
          list->last = list_to_add->last;
          ex_current_was_last = FALSE;
        }
        list_to_add->last->next = next;
        next = prev->next;
      }
    }
    list_to_add->last = NULL;
  }
}


/***********************************************************************
*                          ELIST_ITERATOR::add_list_before
*
*  Insert another list to this list before the current element. Move the
*  iterator to the start of the inserted elements
*  iterator.
**********************************************************************/

template <typename T>
void ELIST_ITERATOR<T>::add_list_before(ELIST<T> *list_to_add) {
#ifndef NDEBUG
  if (!this)
    NULL_OBJECT.error ("ELIST_ITERATOR::add_list_before", ABORT, NULL);
  if (!list)
    NO_LIST.error ("ELIST_ITERATOR::add_list_before", ABORT, NULL);
  if (!list_to_add)
    BAD_PARAMETER.error ("ELIST_ITERATOR::add_list_before", ABORT,
    "list_to_add is NULL");
#endif

  if (!list_to_add->empty ()) {
    if (list->empty ()) {
      list->last = list_to_add->last;
      prev = list->last;
      current = list->First ();
      next = current->next;
      ex_current_was_last = FALSE;
    }
    else {
      prev->next = list_to_add->First ();
      if (current) {             //not extracted
        list_to_add->last->next = current;
      }
      else {                     //current extracted
        list_to_add->last->next = next;
        if (ex_current_was_last)
          list->last = list_to_add->last;
        if (ex_current_was_cycle_pt)
          cycle_pt = prev->next;
      }
      current = prev->next;
      next = current->next;
    }
    list_to_add->last = NULL;
  }
}


/***********************************************************************
*                          ELIST_ITERATOR::extract
*
*  Do extraction by removing current from the list, returning it to the
*  caller, but NOT updating the iterator.  (So that any calling loop can do
*  this.)   The iterator's current points to NULL.  If the extracted element
*  is to be deleted, this is the callers responsibility.
**********************************************************************/

template <typename T>
T *ELIST_ITERATOR<T>::extract() {
  T *extracted_link;

#ifndef NDEBUG
  if (!this)
    NULL_OBJECT.error ("ELIST_ITERATOR::extract", ABORT, NULL);
  if (!list)
    NO_LIST.error ("ELIST_ITERATOR::extract", ABORT, NULL);
  if (!current)                  //list empty or
    //element extracted
    NULL_CURRENT.error ("ELIST_ITERATOR::extract",
    ABORT, NULL);
#endif

  if (list->singleton()) {
    // Special case where we do need to change the iterator.
    prev = next = list->last = NULL;
  } else {
    prev->next = next;           //remove from list

    if (current == list->last) {
      list->last = prev;
      ex_current_was_last = TRUE;
    } else {
      ex_current_was_last = FALSE;
    }
  }
  // Always set ex_current_was_cycle_pt so an add/forward will work in a loop.
  ex_current_was_cycle_pt = (current == cycle_pt) ? TRUE : FALSE;
  extracted_link = current;
  extracted_link->next = NULL;   //for safety
  current = NULL;
  return extracted_link;
}


/***********************************************************************
*                          ELIST_ITERATOR::move_to_first()
*
*  Move current so that it is set to the start of the list.
*  Return data just in case anyone wants it.
**********************************************************************/

template <typename T>
T *ELIST_ITERATOR<T>::move_to_first() {
#ifndef NDEBUG
  if (!this)
    NULL_OBJECT.error ("ELIST_ITERATOR::move_to_first", ABORT, NULL);
  if (!list)
    NO_LIST.error ("ELIST_ITERATOR::move_to_first", ABORT, NULL);
#endif

  current = list->First ();
  prev = list->last;
  next = current ? current->next : NULL;
  return current;
}


/***********************************************************************
*                          ELIST_ITERATOR::mark_cycle_pt()
*
*  Remember the current location so that we can tell whether we've returned
*  to this point later.
*
*  If the current point is deleted either now, or in the future, the cycle
*  point will be set to the next item which is set to current.  This could be
*  by a forward, add_after_then_move or add_after_then_move.
**********************************************************************/

template <typename T>
void ELIST_ITERATOR<T>::mark_cycle_pt() {
#ifndef NDEBUG
  if (!this)
    NULL_OBJECT.error ("ELIST_ITERATOR::mark_cycle_pt", ABORT, NULL);
  if (!list)
    NO_LIST.error ("ELIST_ITERATOR::mark_cycle_pt", ABORT, NULL);
#endif

  if (current)
    cycle_pt = current;
  else
    ex_current_was_cycle_pt = TRUE;
  started_cycling = FALSE;
}


/***********************************************************************
*                          ELIST_ITERATOR::at_first()
*
*  Are we at the start of the list?
*
**********************************************************************/

template <typename T>
bool ELIST_ITERATOR<T>::at_first() {
#ifndef NDEBUG
  if (!this)
    NULL_OBJECT.error ("ELIST_ITERATOR::at_first", ABORT, NULL);
  if (!list)
    NO_LIST.error ("ELIST_ITERATOR::at_first", ABORT, NULL);
#endif

  //we're at a deleted
  return ((list->empty ()) || (current == list->First ()) || ((current == NULL) &&
    (prev == list->last) &&      //NON-last pt between
    !ex_current_was_last));      //first and last
}


/***********************************************************************
*                          ELIST_ITERATOR::at_last()
*
*  Are we at the end of the list?
*
**********************************************************************/

template <typename T>
bool ELIST_ITERATOR<T>::at_last() {
#ifndef NDEBUG
  if (!this)
    NULL_OBJECT.error ("ELIST_ITERATOR::at_last", ABORT, NULL);
  if (!list)
    NO_LIST.error ("ELIST_ITERATOR::at_last", ABORT, NULL);
#endif

  //we're at a deleted
  return ((list->empty ()) || (current == list->last) || ((current == NULL) &&
    (prev == list->last) &&      //last point between
    ex_current_was_last));       //first and last
}


/***********************************************************************
*                          ELIST_ITERATOR::cycled_list()
*
*  Have we returned to the cycle_pt since it was set?
*
**********************************************************************/

template <typename T>
bool ELIST_ITERATOR<T>::cycled_list() {
#ifndef NDEBUG
  if (!this)
    NULL_OBJECT.error ("ELIST_ITERATOR::cycled_list", ABORT, NULL);
  if (!list)
    NO_LIST.error ("ELIST_ITERATOR::cycled_list", ABORT, NULL);
#endif

  return ((list->empty ()) || ((current == cycle_pt) && started_cycling));

}


/***********************************************************************
*                          ELIST_ITERATOR::length()
*
*  Return the length of the list
*
**********************************************************************/

template <typename T>
inT32 ELIST_ITERATOR<T>::length() {
#ifndef NDEBUG
  if (!this)
    NULL_OBJECT.error ("ELIST_ITERATOR::length", ABORT, NULL);
  if (!list)
    NO_LIST.error ("ELIST_ITERATOR::length", ABORT, NULL);
#endif

  return list->length ();
}


/***********************************************************************
*                          ELIST_ITERATOR::sort()
*
*  Sort the elements of the list, then reposition at the start.
*
**********************************************************************/

template <typename T>
void ELIST_ITERATOR<T>::sort (           //sort elements
                      int comparator (                 //comparison routine
                      const void *, const void *)) {
#ifndef NDEBUG
                        if (!this)
                          NULL_OBJECT.error ("ELIST_ITERATOR::sort", ABORT, NULL);
                        if (!list)
                          NO_LIST.error ("ELIST_ITERATOR::sort", ABORT, NULL);
#endif

                        list->sort (comparator);
                        move_to_first();
}


/***********************************************************************
*                          ELIST_ITERATOR::add_to_end
*
*  Add a new element to the end of the list without moving the iterator.
*  This is provided because a single linked list cannot move to the last as
*  the iterator couldn't set its prev pointer.  Adding to the end is
*  essential for implementing
queues.
**********************************************************************/

template <typename T>
void ELIST_ITERATOR<T>::add_to_end(  // element to add
                                       T *new_element) {
#ifndef NDEBUG
                                         if (!this)
                                           NULL_OBJECT.error ("ELIST_ITERATOR::add_to_end", ABORT, NULL);
                                         if (!list)
                                           NO_LIST.error ("ELIST_ITERATOR::add_to_end", ABORT, NULL);
                                         if (!new_element)
                                           BAD_PARAMETER.error ("ELIST_ITERATOR::add_to_end", ABORT,
                                           "new_element is NULL");
                                         if (new_element->next)
                                           STILL_LINKED.error ("ELIST_ITERATOR::add_to_end", ABORT, NULL);
#endif

                                         if (this->at_last ()) {
                                           this->add_after_stay_put (new_element);
                                         }
                                         else {
                                           if (this->at_first ()) {
                                             this->add_before_stay_put (new_element);
                                             list->last = new_element;
                                           }
                                           else {                       //Iteratr is elsewhere
                                             new_element->next = list->last->next;
                                             list->last->next = new_element;
                                             list->last = new_element;
                                           }
                                         }
}

/***********************************************************************
 *							ELIST_ITERATOR::forward
 *
 *  Move the iterator to the next element of the list.
 *  REMEMBER: ALL LISTS ARE CIRCULAR.
 **********************************************************************/

template <typename T>
T *ELIST_ITERATOR<T>::forward() {
  #ifndef NDEBUG
  if (!this)
    NULL_OBJECT.error ("ELIST_ITERATOR::forward", ABORT, NULL);
  if (!list)
    NO_LIST.error ("ELIST_ITERATOR::forward", ABORT, NULL);
  #endif
  if (list->empty ())
    return NULL;

  if (current) {                 //not removed so
                                 //set previous
    prev = current;
    started_cycling = TRUE;
    // In case next is deleted by another iterator, get next from current.
    current = current->next;
  } else {
    if (ex_current_was_cycle_pt)
      cycle_pt = next;
    current = next;
  }
  next = current->next;

  #ifndef NDEBUG
  if (!current)
    NULL_DATA.error ("ELIST_ITERATOR::forward", ABORT, NULL);
  if (!next)
    NULL_NEXT.error ("ELIST_ITERATOR::forward", ABORT,
                     "This is: %p  Current is: %p", this, current);
  #endif
  return current;
}


/***********************************************************************
 *							ELIST_ITERATOR::data_relative
 *
 *  Return the data pointer to the element "offset" elements from current.
 *  "offset" must not be less than -1.
 *  (This function can't be INLINEd because it contains a        loop)
 **********************************************************************/

template <typename T>
T *ELIST_ITERATOR<T>::data_relative(                //get data + or - ...
                                          inT8 offset) {  //offset from current
  T *ptr;

  #ifndef NDEBUG
  if (!this)
    NULL_OBJECT.error ("ELIST_ITERATOR::data_relative", ABORT, NULL);
  if (!list)
    NO_LIST.error ("ELIST_ITERATOR::data_relative", ABORT, NULL);
  if (list->empty ())
    EMPTY_LIST.error ("ELIST_ITERATOR::data_relative", ABORT, NULL);
  if (offset < -1)
    BAD_PARAMETER.error ("ELIST_ITERATOR::data_relative", ABORT,
      "offset < -l");
  #endif

  if (offset == -1)
    ptr = prev;
  else
    for (ptr = current ? current : prev; offset-- > 0; ptr = ptr->next);

  #ifndef NDEBUG
  if (!ptr)
    NULL_DATA.error ("ELIST_ITERATOR::data_relative", ABORT, NULL);
  #endif

  return ptr;
}


/***********************************************************************
 *							ELIST_ITERATOR::move_to_last()
 *
 *  Move current so that it is set to the end of the list.
 *  Return data just in case anyone wants it.
 *  (This function can't be INLINEd because it contains a loop)
 **********************************************************************/

template <typename T>
T *ELIST_ITERATOR<T>::move_to_last() {
  #ifndef NDEBUG
  if (!this)
    NULL_OBJECT.error ("ELIST_ITERATOR<T>::move_to_last", ABORT, NULL);
  if (!list)
    NO_LIST.error ("ELIST_ITERATOR<T>::move_to_last", ABORT, NULL);
  #endif

  while (current != list->last)
    forward();

  return current;
}


/***********************************************************************
 *							ELIST_ITERATOR::exchange()
 *
 *  Given another iterator, whose current element is a different element on
 *  the same list list OR an element of another list, exchange the two current
 *  elements.  On return, each iterator points to the element which was the
 *  other iterators current on entry.
 *  (This function hasn't been in-lined because its a bit big!)
 **********************************************************************/

template <typename T>
void ELIST_ITERATOR<T>::exchange(                             //positions of 2 links
                              ELIST_IT_Type_ *other_it) {  //other iterator
  const ERRCODE DONT_EXCHANGE_DELETED =
    "Can't exchange deleted elements of lists";

  T *old_current;

  #ifndef NDEBUG
  if (!this)
    NULL_OBJECT.error ("ELIST_ITERATOR<T>::exchange", ABORT, NULL);
  if (!list)
    NO_LIST.error ("ELIST_ITERATOR<T>::exchange", ABORT, NULL);
  if (!other_it)
    BAD_PARAMETER.error ("ELIST_ITERATOR<T>::exchange", ABORT, "other_it NULL");
  if (!(other_it->list))
    NO_LIST.error ("ELIST_ITERATOR<T>::exchange", ABORT, "other_it");
  #endif

  /* Do nothing if either list is empty or if both iterators reference the same
  link */

  if ((list->empty ()) ||
    (other_it->list->empty ()) || (current == other_it->current))
    return;

  /* Error if either current element is deleted */

  if (!current || !other_it->current)
    DONT_EXCHANGE_DELETED.error ("ELIST_ITERATOR<T>.exchange", ABORT, NULL);

  /* Now handle the 4 cases: doubleton list; non-doubleton adjacent elements
  (other before this); non-doubleton adjacent elements (this before other);
  non-adjacent elements. */

                                 //adjacent links
  if ((next == other_it->current) ||
  (other_it->next == current)) {
                                 //doubleton list
    if ((next == other_it->current) &&
    (other_it->next == current)) {
      prev = next = current;
      other_it->prev = other_it->next = other_it->current;
    }
    else {                       //non-doubleton with
                                 //adjacent links
                                 //other before this
      if (other_it->next == current) {
        other_it->prev->next = current;
        other_it->current->next = next;
        current->next = other_it->current;
        other_it->next = other_it->current;
        prev = current;
      }
      else {                     //this before other
        prev->next = other_it->current;
        current->next = other_it->next;
        other_it->current->next = current;
        next = current;
        other_it->prev = other_it->current;
      }
    }
  }
  else {                         //no overlap
    prev->next = other_it->current;
    current->next = other_it->next;
    other_it->prev->next = current;
    other_it->current->next = next;
  }

  /* update end of list pointer when necessary (remember that the 2 iterators
    may iterate over different lists!) */

  if (list->last == current)
    list->last = other_it->current;
  if (other_it->list->last == other_it->current)
    other_it->list->last = current;

  if (current == cycle_pt)
    cycle_pt = other_it->cycle_pt;
  if (other_it->current == other_it->cycle_pt)
    other_it->cycle_pt = cycle_pt;

  /* The actual exchange - in all cases*/

  old_current = current;
  current = other_it->current;
  other_it->current = old_current;
}


/***********************************************************************
 *							ELIST_ITERATOR::extract_sublist()
 *
 *  This is a private member, used only by ELIST::assign_to_sublist.
 *  Given another iterator for the same list, extract the links from THIS to
 *  OTHER inclusive, link them into a new circular list, and return a
 *  pointer to the last element.
 *  (Can't inline this function because it contains a loop)
 **********************************************************************/

template <typename T>
T *ELIST_ITERATOR<T>::extract_sublist(                             //from this current
                                            ELIST_IT_Type_ *other_it) {  //to other current
  #ifndef NDEBUG
  const ERRCODE BAD_EXTRACTION_PTS =
    "Can't extract sublist from points on different lists";
  const ERRCODE DONT_EXTRACT_DELETED =
    "Can't extract a sublist marked by deleted points";
  #endif
  const ERRCODE BAD_SUBLIST = "Can't find sublist end point in original list";

  ELIST_ITERATOR temp_it = *this;
  T *end_of_new_list;

  #ifndef NDEBUG
  if (!this)
    NULL_OBJECT.error ("ELIST_ITERATOR<T>::extract_sublist", ABORT, NULL);
  if (!other_it)
    BAD_PARAMETER.error ("ELIST_ITERATOR<T>::extract_sublist", ABORT,
      "other_it NULL");
  if (!list)
    NO_LIST.error ("ELIST_ITERATOR<T>::extract_sublist", ABORT, NULL);
  if (list != other_it->list)
    BAD_EXTRACTION_PTS.error ("ELIST_ITERATOR<T>.extract_sublist", ABORT, NULL);
  if (list->empty ())
    EMPTY_LIST.error ("ELIST_ITERATOR<T>::extract_sublist", ABORT, NULL);

  if (!current || !other_it->current)
    DONT_EXTRACT_DELETED.error ("ELIST_ITERATOR<T>.extract_sublist", ABORT,
      NULL);
  #endif

  ex_current_was_last = other_it->ex_current_was_last = FALSE;
  ex_current_was_cycle_pt = FALSE;
  other_it->ex_current_was_cycle_pt = FALSE;

  temp_it.mark_cycle_pt ();
  do {                           //walk sublist
    if (temp_it.cycled_list ())  //cant find end pt
      BAD_SUBLIST.error ("ELIST_ITERATOR<T>.extract_sublist", ABORT, NULL);

    if (temp_it.at_last ()) {
      list->last = prev;
      ex_current_was_last = other_it->ex_current_was_last = TRUE;
    }

    if (temp_it.current == cycle_pt)
      ex_current_was_cycle_pt = TRUE;

    if (temp_it.current == other_it->cycle_pt)
      other_it->ex_current_was_cycle_pt = TRUE;

    temp_it.forward ();
  }
  while (temp_it.prev != other_it->current);

                                 //circularise sublist
  other_it->current->next = current;
  end_of_new_list = other_it->current;

                                 //sublist = whole list
  if (prev == other_it->current) {
    list->last = NULL;
    prev = current = next = NULL;
    other_it->prev = other_it->current = other_it->next = NULL;
  }
  else {
    prev->next = other_it->next;
    current = other_it->current = NULL;
    next = other_it->next;
    other_it->prev = prev;
  }
  return end_of_new_list;
}

#define ELISTIZE(CLASSNAME)\
  typedef ELIST<CLASSNAME> CLASSNAME##_LIST;\
  typedef ELIST_ITERATOR<CLASSNAME> CLASSNAME##_IT;

#endif
